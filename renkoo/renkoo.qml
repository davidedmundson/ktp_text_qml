// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    width: 360
    height: 360

    //    MouseArea {
    //        anchors.fill: parent
    //        onClicked: {
    ////            Qt.quit();
    //        }
    //    }

    ListModel {
        id: myModel
        ListElement { direction: 0; message: "Hey!"; failedToSend: false}
        ListElement { direction: 1; message: "Hi"; failedToSend: false}
        ListElement { direction: 1; message: "What is the airspeed velocity of an unladen swallow?"; failedToSend:true}
        ListElement { direction: 0; message: "ASDFSAF!";  failedToSend: false}

    }

    Header {
        id:header
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        z: 1000
    }


    ListView{
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 20

        spacing: 5

        model: myModel
        //        interactive: false

        delegate: Message{
        }

    }
}
