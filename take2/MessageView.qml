import QtQuick 2.0
import QtQuick.Controls 1.1

Item {
    property alias messagesModel: listView.model
    
    Header {
        id:header
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        z: 1000
    }

    ScrollView {
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        ListView {
            id:listView
            property bool followConversation: true

            delegate: Loader {
                Component.onCompleted: {
                    switch(model.messageType) {
                    case 1:
                        source = "ActionDelegate.qml";
                        break
                    default:
                        source = "MessageDelegate.qml";
                        break;
                    }
                }
            }
            boundsBehavior: Flickable.StopAtBounds

            add: Transition {
                NumberAnimation { properties: "opacity"; from: 0; to: 1.0; duration: 500 }
            }


            //The following is /all/ coode to make sure we
            //scroll to the end on receipt of a new item
            //if and only if the user is currently already at the end of the view

            //it may look like there is a much saner simpler way to do this but there really isn't

            //onCountChanged can't be used to know when we have a new item, as the item wont' exist yet
            //hence using onContentHeightChanged

            //positionViewAtEnd starts an animation
            //there's no way to know when that's done, hence tracking onContentY
            //timers are because the internal vData struct hasn't changed when it updates these signals

            //don't change this. Ever.
            // - David

            onContentYChanged: {
                checkAtEndTimer.running = true;
            }

            onContentHeightChanged: {
                moveToEndTimer.running = true;
            }

            Timer {
                id: moveToEndTimer
                interval: 0
                onTriggered: {
                    if(listView.followConversation) {
                        listView.positionViewAtEnd();
                    }
                }
            }

            Timer {
                id: checkAtEndTimer
                interval: 10
                onTriggered: {
                    listView.followConversation = listView.atYEnd
                }
            }
        }
    }
}
