import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1


//this is bodgy code for test purposes

Rectangle
{
    color: "white"
    width: 800
    height: 600
    
    GridLayout {
        anchors.fill: parent
        columns: 2
        MessageView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            messagesModel: myModel
        }
        ColumnLayout {
            Layout.rowSpan: 2
            Button {
                text: "Send message"
                onClicked: {
                    addMessage(1, "Hello", "Some Bloke")
                }
            }
            Button {
                text: "Receive message"
                onClicked: {
                    addMessage(0, "Hi", "David Edmundson")
                }
            }
            Button {
                text: "Last message is delivered"
            }
            Button {
                text: "Last message is error"
            }
            
            Button {
                id: spamButton
                text: "Spam"
                checkable: true
                
            }
            
        }
        TextField {
            Layout.fillWidth: true
            Layout.minimumHeight: 40
        }        
    }
    
    function addMessage(_direction, _message, _senderName) {
        myModel.append({ direction: _direction, message: _message, sender: _senderName, failedToSend: false});
    }
    
    ListModel {
        id: myModel
        ListElement { direction: 1; message: "Hey!"; failedToSend: false; sender: "Martin Klapetek"}
        ListElement { direction: 1; message: "Check out <a href=\"http://foo.co.uk\">http://foo.co.uk/myImage</a>" ; failedToSend: false ; sender: "Martin Klapetek"; image:"http://b.vimeocdn.com/ts/201/743/201743480_640.jpg"}
        ListElement { direction: 0; message: "Dude, that's <b>awesome!</b>"; failedToSend:true ; sender: "David Edmundson"}
        ListElement { messageType: 1; message: "Dan Vratil has joined the room";}
        ListElement { direction: 1; message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec accumsan sagittis dictum. In mauris massa, fringilla quis diam consequat, tincidunt consectetur tortor. Mauris lobortis scelerisque ante, a imperdiet leo placerat vitae. Donec mattis nunc quam, auctor pulvinar urna hendrerit sed. In malesuada porttitor arcu, ut venenatis dui commodo in. Nulla sed dolor eleifend, cursus nulla sed, tristique dolor. Vivamus non condimentum orci. Quisque nibh dui, adipiscing in porta at, molestie ut libero. Ut at ultrices libero. Phasellus quis rhoncus nibh. Donec pulvinar urna ut felis gravida, sed porta libero volutpat. Nullam placerat imperdiet nunc, sed consequat mi sodales non."; failedToSend:true ; sender: "Martin Klapetek"}
        ListElement { direction: 0; message: "ASDFSAF!";  failedToSend: false ; sender: "David Edmundson"}
    }
    
    
    /*
     * 
     *     roles[TextRole] = "text";
    roles[TimeRole] = "time";
    roles[TypeRole] = "type";
    roles[SenderIdRole] = "senderId";
    roles[SenderAliasRole] = "senderAlias";
    roles[SenderAvatarRole] = "senderAvatar";
    roles[DeliveryStatusRole] = "deliveryStatus";
    roles[DeliveryReportReceiveTimeRole] = "deliveryReportReceiveTime";
     */
//         enum MessageType {
//         MessageTypeIncoming,
//         MessageTypeOutgoing,
//         MessageTypeAction,
//         MessageTypeNotice
//     };
// 
//     enum DeliveryStatus {
//         DeliveryStatusUnknown,
//         DeliveryStatusDelivered,
//         DeliveryStatusRead, // implies DeliveryStatusDelivered
//         DeliveryStatusFailed
//     };
// 

    
    Timer {
            interval: 500;
            running: spamButton.checked
            repeat: true
            triggeredOnStart: true
            onTriggered: {
                addMessage(0, "SPAM SPAM SPAM", "David Edmundson");
            }
    }
    
}